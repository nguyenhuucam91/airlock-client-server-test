// import Dashboard from '../pages/Dashboard.vue';
// import Login from '../pages/Login.vue';
import VueRouter from 'vue-router';

const routes = [
  { path: '/', component: () => import('../pages/Login.vue') },
  { path: '/dashboard', component: () => import('../pages/Dashboard.vue') }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router;